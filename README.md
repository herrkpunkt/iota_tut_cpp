# IOTA Tutorial C++
**NEVER USE YOUR TOKENS SEED IN ANY OF THESE EXAMPLES!!!!!!!!**
## How to build
This project has to be built with bazel: `bazel build //...`
## How to use this project
* Address Generator:
`./bazel-bin/apps/GenerateAddress`
Flags: -seed (default: 9999...), -first_index (default: 0), -last_index (default: 0), -security (default: 2)