//
// Copyright (c) 2029 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#ifndef ADDRGEN_H
#define ADDRGEN_H

#include <string>
#include "common/helpers/sign.h"
#include "common/trinary/tryte.h"

class AddrGen{
 public:
  AddrGen();
  ~AddrGen();
  std::string getAddress(std::string, unsigned int, unsigned int);
};

#endif // ADDRGEN_H
