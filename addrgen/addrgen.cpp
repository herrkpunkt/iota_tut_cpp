//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "addrgen.h"

AddrGen::AddrGen(){
}

AddrGen::~AddrGen(){
}

std::string AddrGen::getAddress(std::string _seed, unsigned int _security, unsigned int _index){
  tryte_t const * const  SEED = (tryte_t*) _seed.c_str();
  char* out_1 = iota_sign_address_gen_trytes((char*)SEED, _index, _security);
  std::string result = std::string(out_1);
  free(out_1);
  return result;
}
