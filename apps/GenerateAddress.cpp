//
// Copyright (c) 2020 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#define DEFAULT_SEED "999999999999999999999999999999999999999999999999999999999999999999999999999999999"
#define DEFAULT_SECURITY 2
#define DEFAULT_FIRST_INDEX 0
#define DEFAULT_LAST_INDEX 0
#define SEED_LENGTH 81

#include <iostream>
#include <sstream>
#include <chrono>
#include <vector>
#include <algorithm>
#include "addrgen/addrgen.h"

static const bool containsOnlyTrytes(std::string _toTest) {
  std::string alphabetString(TRYTE_ALPHABET);
  std::vector<char> alphabetVector(alphabetString.begin(),
                                   alphabetString.end());
  for (unsigned int i = 0; i < _toTest.length(); i++) {
    if (std::find(alphabetVector.begin(), alphabetVector.end(),
                  _toTest.at(i)) == alphabetVector.end()) {
      return false;
    }
  }
  return true;
}

static const bool isValid(unsigned int _length, std::string _toTest) {
  if (!containsOnlyTrytes(_toTest))
    return false;
  if (_toTest.length() != _length)
    return false;
  return true;
}

int main(int argc, const char *argv[]) {
  unsigned int security = DEFAULT_SECURITY;
  std::string seed = DEFAULT_SEED;
  unsigned int first_index = DEFAULT_FIRST_INDEX;
  unsigned int last_index = DEFAULT_LAST_INDEX;
  for(int i = 0; i < argc; i++){
    if(std::string(argv[i]).compare(std::string("-security")) == 0){
      std::stringstream strStream(std::string(argv[i+1]));
      strStream >> security;
    }
    else if(std::string(argv[i]).compare(std::string("-seed")) == 0){
      std::stringstream strStream(std::string(argv[i+1]));
      strStream >> seed;
    }
    else if(std::string(argv[i]).compare(std::string("-first_index")) == 0){
      std::stringstream strStream(std::string(argv[i+1]));
      strStream >> first_index;
    }
    else if(std::string(argv[i]).compare(std::string("-last_index")) == 0){
      std::stringstream strStream(std::string(argv[i+1]));
      strStream >> last_index;
    }
  }
  if(!isValid(SEED_LENGTH, seed)){
    std::cout << "Invalid Seed!" << std::endl;
    return 0;
  }
  if(last_index < first_index || last_index < 0 || first_index < 0){
    std::cout << "Invalid indezes!" << std::endl;
    return 0;
  }
  if(security > 3 || security < 1){
    std::cout << "Invalid security!" << std::endl;
    return 0;
  }
  AddrGen * gen = new AddrGen();
  auto start = std::chrono::high_resolution_clock::now();
  for(unsigned int i = first_index; i <= last_index; i++){
    std::cout << "Address for index " << i << ": " << gen->getAddress(seed, security, i) << std::endl;
  }
  auto finish = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsed = finish - start;
  std::cout << "Generated " << std::to_string(last_index - first_index + 1) << " address(es) in " << elapsed.count() << " seconds" << std::endl; 
  delete(gen);
  return 0;
}
